#include "iostream"
#include "ctime"
#include "vector"
#include "chrono"

// Структура для под массивов
struct RunCoord
{
	int Left, Right;
};

// Длина одного подмассива Run 
int RunLenghth(RunCoord Run)
{
	return Run.Right - Run.Left + 1;
}

// Для облегчения сортировки
void InsertionSort(std::vector<int> &Vector, int Left, int Right)
{
	for (int i = 1; i < Right - Left + 1; i++)
	{
		int j = Left + i;
		while (j > Left && Vector[j - 1] > Vector[j])
		{
			int c = Vector[j - 1];
			Vector[j - 1] = Vector[j];
			Vector[j] = c;
			j--;
		}
	}
}

// Метод для обьединения подмассивов
void Merge(std::vector<int> &Vector, RunCoord LeftArr, RunCoord RightArr)
{
	std::vector<int> Buffer(RunLenghth(LeftArr));

	for (int i = 0; i < RunLenghth(LeftArr); i++)
	{
		Buffer[i] = Vector[LeftArr.Left + i];
	}
	
	int IndexBig = RightArr.Left, IndexTemp = 0, z = LeftArr.Left;
	while (IndexTemp <= Buffer.size() - 1 and IndexBig <= RightArr.Right)
	{
		if (Buffer[IndexTemp] <= Vector[IndexBig])
		{
			Vector[z] = Buffer[IndexTemp];
			IndexTemp++;
			z++;
		}
		else
		{
			Vector[z] = Vector[IndexBig];
			IndexBig++;
			z++;
		}
	}
	if (IndexTemp == Buffer.size())
	{
		while (IndexBig <= RightArr.Right)
		{
			Vector[z] = Vector[IndexBig];
			IndexBig++;
			z++;
		}
	}
	else if (IndexBig == RightArr.Right + 1) {
		while (IndexTemp <= Buffer.size() - 1)
		{
			Vector[z] = Buffer[IndexTemp];
			IndexTemp++;
			z++;
		}
	}

}

// Минимальный размер упорядоченной последовательности
int GetMinrun(int n)
{
	int r = 0; /* станет 1 если среди сдвинутых битов будет хотя бы 1 ненулевой */
	while (n >= 64)
	{
		r |= n & 1;
		n >>= 1;
	}
	return n + r;
}

// Алгоритм сортировки
void Timsort(std::vector<int> &Vector, int MinRun, int ListN)
{
	int Temp = 0;
	RunCoord CurrentRun;
	std::vector<RunCoord> Stack;

	while (Temp < ListN - 2)
	{
		int Left, Right;

		Left = Temp;
		Temp++;
		if (Vector[Temp - 1] > Vector[Temp])
		{
			while (Temp + 1 < ListN)
			{ 
				if (Vector[Temp] > Vector[Temp + 1])
				{
					Temp++;
				}
				else
				{
					break;
				}
			}
			Right = Temp;
			for (int i = Left; i < (Right - Left + 1) / 2; i++)
			{
				int c = Vector[i];
				Vector[i] = Vector[Right - i];
				Vector[Right - i] = c;
			}
		}
		else
		{
			while (Temp + 1 < ListN)
			{
				if (Vector[Temp] <= Vector[Temp + 1])
				{
					Temp++;
				}
				else { break; }
			}
			Right = Temp;
		}
		if ((Right - Left + 1) < MinRun)
		{
			int z = (Right - Left + 1);
			for (int i = 0; i < MinRun - z; i++)
			{
				if (Temp >= ListN - 1)
				{
					break;
				}
				Temp++; 
			}
		}
		Right = Temp;
		InsertionSort(Vector, Left, Right);
		Temp++;
		CurrentRun.Left = Left;
		CurrentRun.Right = Right;
		Stack.push_back(CurrentRun);
		bool first = 0, second = 0, truth = 0;
		while (Stack.size() > 1 and truth == 0)
		{
			int n = Stack.size() - 2;
			if (Stack.size() > 1 and RunLenghth(Stack[n]) <= RunLenghth(Stack[n + 1]))
			{
				Merge(Vector, Stack[n], Stack[n + 1]);
				Stack[n].Right = Stack[n + 1].Right;
				Stack.erase(Stack.begin() + n + 1);
				n--;
			}
			else if (Stack.size() > 2 and RunLenghth(Stack[n - 1]) <= RunLenghth(Stack[n]) + RunLenghth(Stack[n + 1]))
			{
				if (RunLenghth(Stack[n - 1]) <= RunLenghth(Stack[n + 1]))
				{
					Merge(Vector, Stack[n - 1], Stack[n]);
					Stack[n - 1].Right = Stack[n].Right;
					Stack.erase(Stack.begin() + n);
					n--;
				}
				else
				{
					Merge(Vector, Stack[n], Stack[n + 1]);
					Stack[n].Right = Stack[n + 1].Right;
					Stack.erase(Stack.begin() + 2);
					n--;
				}
			}
			else if (Temp == ListN)
			{
				if (RunLenghth(Stack[n]) >= RunLenghth(Stack[n + 1]))
				{
					Merge(Vector, Stack[n], Stack[n + 1]);
					Stack[n].Right = Stack[n + 1].Right;
					Stack.erase(Stack.begin() + n + 1);
				}
			}
			if (Stack.size() > 1 and (Temp != ListN) and RunLenghth(Stack[n]) > RunLenghth(Stack[n + 1]))
			{
				first = 1;
				if (Stack.size() < 3)
				{
					second = 1;
				}
				else if (Stack.size() > 2 and RunLenghth(Stack[n - 1]) > RunLenghth(Stack[n]) + RunLenghth(Stack[n + 1]))
				{
					second = 1;
				}
				if (first and second) truth = 1;
			}
			
			
		}
	}

}

int main() {
	srand(time(0));
	int Minrun, Num;
	std::cout << "Number of elements: ";
	
	std::cin >> Num;
	std::vector<int> Vector(Num);

	for (int i = 0; i < Num; i++)
	{
		Vector[i] = (rand() % 10);
	}
	Minrun = GetMinrun(Num);
	auto Begin = std::chrono::high_resolution_clock::now();
	Timsort(Vector, Minrun, Num);
	auto End = std::chrono::high_resolution_clock::now();
	std::cout << "Time work: "<<(std::chrono::duration_cast<std::chrono::nanoseconds>(End - Begin).count()) / 1000000 << "ms" << std::endl;
}