#include "iostream"
#include "CircularBufferOne.h"

int main()
{
    CircularBufferOne BufferOne;

    int Count;
    std::cin >> Count;

    for (int i = 1; i <= Count; ++i)
    {
        BufferOne.Add(i);
        std::cout << "Create space:" << BufferOne.GetFreeSpace() << "; Occupied:" << BufferOne.GetOccupiedSpace() << std::endl;
    }

    for (int i = 0; i < Count; ++i)
    {
        std::cout << "Get next:" << BufferOne.Get() << std::endl;
        std::cout << "Space:" << BufferOne.GetFreeSpace() << "; Occupied:" << BufferOne.GetOccupiedSpace() << std::endl;
    }
    return 0;
}
