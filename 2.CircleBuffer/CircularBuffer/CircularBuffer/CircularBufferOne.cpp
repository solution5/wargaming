﻿#include "CircularBufferOne.h"

CircularBufferOne::CircularBufferOne()
{
    // Инициализация мета под буффер.
    Size = 1024;
    Data = new int[Size];
    // Сборс внутреннего состояния всех полей.
    FreeSpace = Size;
    OccupiedSpace = 0;
}

CircularBufferOne::CircularBufferOne(int mySize)
{
    // Инициализация мета под буффер.
    Size = mySize;
    Data = new int[Size];
    // Сборс внутреннего состояния всех полей.
    FreeSpace = Size;
    OccupiedSpace = 0;
}

int CircularBufferOne::GetFreeSpace()
{
    return Size - OccupiedSpace;
}

int CircularBufferOne::GetOccupiedSpace()
{
    return OccupiedSpace;
}

void CircularBufferOne::Add(int Value)
{
    Data[OccupiedSpace++ % Size] = Value;
}

bool CircularBufferOne::isEmptyBuffer()
{
    if (OccupiedSpace == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int CircularBufferOne::Get()
{
    if (isEmptyBuffer())
    {
        throw "Buffer is empty!";
        return 0;
    }

    int FirstValue = Data[0];

    for (int i = 1; i < OccupiedSpace; ++i)
    {
        Data[i - 1] = Data[i];
    }
    --OccupiedSpace;

    return FirstValue;
}

CircularBufferOne::~CircularBufferOne()
{
    delete[] Data;
}
