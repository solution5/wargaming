﻿#include "iostream"
#include "CircularBufferTwo.h"

CircularBufferTwo::CircularBufferTwo()
{
    // Инициализация мета под буффер.
    Size = 1024;
    Data = new int[Size];
    // Сборс внутреннего состояния всех полей.
    FreeSpace = Size;
    OccupiedSpace = 0;
}

CircularBufferTwo::CircularBufferTwo(int mySize)
{
    // Инициализация мета под буффер.
    Size = mySize;
    Data = new int[Size];
    // Сборс внутреннего состояния всех полей.
    FreeSpace = Size;
    OccupiedSpace = 0;
}

int CircularBufferTwo::GetFreeSpace()
{
    return Size - OccupiedSpace;
}

int CircularBufferTwo::GetOccupiedSpace()
{
    return OccupiedSpace;
}

void CircularBufferTwo::Add(int Value)
{
    if (isFullFuffer())
    {
        throw "Buffer is full!";
        return;
    }
    Data[OccupiedSpace++ % Size] = Value;
}

bool CircularBufferTwo::isEmptyBuffer()
{
    if (OccupiedSpace == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool CircularBufferTwo::isFullFuffer()
{
    if (FreeSpace == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int CircularBufferTwo::Get()
{
    if (isEmptyBuffer())
    {
        throw "Buffer is empty!";
        return 0;
    }

    int FirstValue = Data[0];

    for (int i = 1; i < OccupiedSpace; ++i)
    {
        Data[i - 1] = Data[i];
    }
    --OccupiedSpace;

    return FirstValue;
}

CircularBufferTwo::~CircularBufferTwo()
{
    delete[] Data;
}
