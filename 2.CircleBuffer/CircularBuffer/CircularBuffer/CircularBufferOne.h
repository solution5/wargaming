﻿#pragma once

class CircularBufferOne {
private:
    int* Data; // Хранимые данные.
    int Size; // Размер циклического буффера.
    int FreeSpace; // Свободное метос в буффере.
    int OccupiedSpace; // Занятое мето.

public:
    // Конструктор по умолчанию
    CircularBufferOne();
    // Коструктор с заданым парамеьром
    CircularBufferOne(int mySize);
 
    // Метод, возвращающий свободное место.
    int GetFreeSpace();
 
    // Метод, возвращающий занятое место.
    int GetOccupiedSpace();
 
    // Метод добавления элемента в буффер.
    void Add(int Value);

    // Метод проверки пустой ли буффер.
    bool isEmptyBuffer();
 
    // Метод достающий элемент из очереди (с последующим его выталкиванием).
    int Get();

    // Деструктор
    ~CircularBufferOne();
};