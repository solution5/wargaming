﻿#pragma once

class CircularBufferTwo
{
    int* Data;          // Хранимые данные.
    int Size;          // Размер циклического буффера.
    int FreeSpace;     // Свободное метос в буффере.
    int OccupiedSpace; // Занятое мето.
public:
    // Конструктор по умолчанию
    CircularBufferTwo();
    // Коструктор с заданым парамеьром
    CircularBufferTwo(int mySize);
 
    // Метод, возвращающий свободное место.
    int GetFreeSpace();
 
    // Метод, возвращающий занятое место.
    int GetOccupiedSpace();
 
    // Метод добавления элемента в буффер.
    void Add(int Value);

    // Метод проверки пустой ли буффер.
    bool isEmptyBuffer();

    // Метод проверки заполнен ли буффер.
    bool isFullFuffer();
 
    // Метод достающий элемент из очереди (с последующим его выталкиванием).
    int Get();

    // Деструктор
    ~CircularBufferTwo();
};
