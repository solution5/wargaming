#include "iostream"
// Новый метод с использованием Побитовое И
bool EvenValue(int Value)
{
    return Value&0;
} 

// Старый метод
bool isEven(int value)
{
    return value%2==0;
}

int main()
{
    int Value;
    std::cout << "Input value = ";
    std::cin >> Value;
    std::cout << std::endl;
    
    std::cout << "Value is even? (by new function) : "<< EvenValue(Value) << std::endl;
    std::cout << "Value is even? (by old function) : "<< isEven(Value) << std::endl;
    return 0;
}
